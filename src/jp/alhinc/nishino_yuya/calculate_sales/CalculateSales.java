package jp.alhinc.nishino_yuya.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


// 次回、Java82行目からスタートする
//→readLineメソッドによる文章の読み込み
public class CalculateSales {

	public static void main(String[] args) {
		// 支店コードと売上金、支店コードと支店名別のMapを宣言
		Map<String, String> branch = new HashMap<String, String>();
		Map<String, Long> branchAmount = new HashMap<String, Long>();
		BufferedReader br = null;

		// 今回のディレクトリを指定し、各ファイルを配列に格納
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		try {
			// 処理1-1 支店定義ファイルを開く
			// 開きたいファイルパスをインスタンスとして宣言　※引数の分、パスが深くなっていく
			File file = new File(args[0], "branch.lst");

			// エラー処理1-1
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			// 処理1-2 支店番号と支店名の紐づけ
			// ファイルから文字を受け取るインスタンスを生成
			FileReader fr = new FileReader(file);
			// 受け取った文字列を貯蔵するための、インスタンスを生成
			br = new BufferedReader(fr);

			//読み込んだファイルを、文章が入っているところまで一行ずつ保持する
			String line;
			String[] branchNames;
			while((line = br.readLine()) != null) {
				// 支店コードと支店名を分割→支店名、売り上げを2つのマップに保持
				// かつエラー処理1-2
				branchNames = line.split(",", -1);
				if((!(branchNames[0].matches("^[0-9]{3}$"))) || branchNames.length != 2 ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if(branchNames[1].matches("")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branch.put(branchNames[0], branchNames[1]);
				branchAmount.put(branchNames[0], 0L);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			// fileを閉じる処理
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		// 売り上げファイルのみのパス、が格納されたリスト作成
		ArrayList<File> salesFiles = new ArrayList<File>();
		File branchDir = new File(args[0]);
		File[] branchDirFiles = branchDir.listFiles();
		for(int i = 0; i<branchDirFiles.length; i++) {
			if((branchDirFiles[i].getName().matches("^[0-9]{8}.rcd$")) && (branchDirFiles[i].isFile())) {
				salesFiles.add(branchDirFiles[i]);
			}
		}

		// エラー処理2-1
		for(int i=0; i<salesFiles.size() - 1; i++) {
			String fileName1 = salesFiles.get(i).getName().substring(0, 8);
			String fileName2 = salesFiles.get(i + 1).getName().substring(0, 8);
			int intSalesFileName1 = Integer.parseInt(fileName1);
			int intSalesFileName2 = Integer.parseInt(fileName2);
			// 連番チェック
			if(intSalesFileName1 != (intSalesFileName2 - 1)) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		// 売り上げファイルを一つずつ読み込む(ループ文)
		for(int i=0; i<salesFiles.size(); i++) {
			try {
				File salesFile = new File(args[0], salesFiles.get(i).getName());
				FileReader salesFr = new FileReader(salesFile);
				br = new BufferedReader(salesFr);


				//読み込んだファイルの情報をリストに突っ込む&エラー処理2-4
				String line;
				ArrayList<String> sales = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					sales.add(line);
				}
				if(sales.size() !=2) {
					System.out.println(salesFile.getName() + "のフォーマットが不正です");
					return;
				}
				// エラー処理、売り上げ金額に数字以外が入ってきた時の処理
				if(!sales.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				// エラー処理2-3
				if(!branch.containsKey(sales.get(0))) {
					System.out.println(salesFile.getName() + "の支店コードが不正です");
					return;
				}

				Long salesAmount = Long.parseLong(sales.get(1));
				salesAmount += branchAmount.get(sales.get(0));

				// エラー処理2－2
				String strSalesAmount = String.valueOf(salesAmount);
				if(!strSalesAmount.matches("[0-9]{1,10}")) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchAmount.put(sales.get(0), salesAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				// saes_fileのclose
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

			// 出力処理メソッドの呼び出し
			if(!outputSales(args[0],"branch.out", branch, branchAmount)) {
				return;
			}
		}
	}
	public static boolean outputSales(String args,String fileName, Map<String, String> name, Map<String, Long> sales) {
		// 処理3
		File resultFile = new File(args, fileName);
		BufferedWriter bw = null;
		try {
		// Fileに文字列を渡すインスタンスを作成
		FileWriter fw = new FileWriter(resultFile);
		// 受け取った文字列を貯めておくインスタンスを作成
		bw = new BufferedWriter(fw);

		// 拡張for文による値の出力
		for(String num : sales.keySet()) {
			bw.write(num + "," + name.get(num) + "," + sales.get(num));
			bw.newLine();
		}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
